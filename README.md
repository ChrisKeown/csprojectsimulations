## ABOUT

This repository contains all code for cell proliferation simulations implemented using software design patterns (**Abstract Factory**, **Builder**, **Prototype**), as part of my 3rd year Computer Science Project. Each simulation does the same thing - the cells have a chance to create a new cell of the same type via the design pattern.

The **Generic Simulation** uses the Abstract Factory pattern to implement three simple cell behaviours: _divide_, _differentiate_ or _die_, when in the right state.

The Mason library is included as its needed to run/edit the code. Add it to your CLASSPATH/as a User Library in eclipse (easier way).

The JavaDoc for MASON can be found [here](https://cs.gmu.edu/~eclab/projects/mason/docs/classdocs/index.html "MASON JavaDoc")

## TO RUN

Import folder as a Java project into eclipse, and run simNameWITHUI as a Java Application.

Or compile simNameWITHUI.java and run from cmd line (may need to move /bin files into /src folder or vice versa)