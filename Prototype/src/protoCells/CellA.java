package protoCells;

import sim.engine.SimState;
/**
 * The Concrete Product<br>
 * The A-Type cells
 * @author Chris Keown
 *
 */
public class CellA extends Cell{
	private static final long serialVersionUID = 1L;
	/**
	 * Cell Constructor
	 */
	public CellA(){
			super(CellType.A);
	}
	/**
	 * @return the cell type as String
	 */
	@Override
	public String toString(){
		return "A";
	}
	/**
	 * Method used to clone the cell
	 */
	@Override
	public Cell clone(SimState state) throws CloneNotSupportedException{
		return super.clone(state);
	}
}
