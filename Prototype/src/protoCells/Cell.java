package protoCells;

import protoMain.CellSim;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
/**
 * The Abstract Prototype
 * @author Chris Keown
 *
 */
public class Cell implements PrototypeInterface, Steppable, Stoppable {
	private static final long serialVersionUID = 1L;
	/**
	 * The cells stopper
	 */
	protected Stoppable stopper;
	/**
	 * The cells type
	 */
	private CellType cellType;
	/**
	 * Constructor for the cell
	 */
	public Cell(CellType type){
		this.cellType = type;
	}
	/**
	 * Set the cells type
	 * @param t - the type
	 */
	public void setType(CellType t){
		this.cellType = t;
	}
	/**
	 * 
	 * @return the cells type
	 */
	public CellType getType(){
		return cellType;
	}

	/**
	 * Set the cells stopper
	 * @param stop - the cells new stopper
	 */
	public void setStopper(Stoppable stop){
		this.stopper = stop;
	}
	/**
	 * Stop the cell from appearing on the scheduler
	 */
	@Override
	public void stop(){
		stopper.stop();
	}
	/**
	 * Clone the cell.<br> The cell needs to be put a new location on the grid, and added to the correct count bag
	 */
	@Override
	public Cell clone(SimState state) throws CloneNotSupportedException{
		Cell cloned = (Cell)super.clone();
		//put cloned cell at new random location and set stopper
		((CellSim)state).addCellToGrid(cloned);
		//add cell to correct count Bag
		if (cloned.getType() == CellType.A){
			((CellSim)state).cellABag.add(cloned);
		}
		else if (cloned.getType() == CellType.B){
			((CellSim)state).cellBBag.add(cloned);
		}
		return cloned;
	}
	
	
	/**
	 * Method to be executed on each step of the scheduler<br>
	 * Each cell has a 1% chance to clone itself, child cell appears randomly onto the cell grid
	 */
	@Override
	public void step(SimState state) {
		CellSim cSim = (CellSim) state;
		if (cSim.schedule.getSteps() == cSim.totalSteps){
			this.stop();
		}
		else{
			int ran = cSim.random.nextInt(100);
			if (ran==0){
				try{
					this.clone(state);
				}
				catch (CloneNotSupportedException e){
					e.printStackTrace();
				}
			}
		}
	}

}
