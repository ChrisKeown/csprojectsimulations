package protoCells;
/**
 * Enum of the cell types
 * @author Chris Keown
 *
 */
public enum CellType {
	A,B;
}
