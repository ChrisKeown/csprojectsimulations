package protoCells;

import sim.engine.SimState;
/**
 * The cloneable interface.
 * @author Chris
 *
 */
public interface PrototypeInterface extends Cloneable {
	/**
	 * The abstract method for cloning
	 * @param state - current state of the sim
	 * @return a cloned cell
	 * @throws CloneNotSupportedException
	 */
	public PrototypeInterface clone(SimState state) throws CloneNotSupportedException;
}
