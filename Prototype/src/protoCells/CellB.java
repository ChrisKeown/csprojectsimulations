package protoCells;

import sim.engine.SimState;
/**
 * The Concrete Product<br>
 * The B-Type Cells
 * @author Chris Keown
 *
 */
public class CellB extends Cell{
	private static final long serialVersionUID = 1L;
	/**
	 * Cell Constructor
	 */
	public CellB(){
		super(CellType.B);
	}
	/**
	 * @return the cell type as String
	 */
	@Override
	public String toString(){
		return "B";
	}
	/**
	 * Method to clone the cell
	 */
	@Override
	public Cell clone(SimState state) throws CloneNotSupportedException{
		return super.clone(state);
	}

}
