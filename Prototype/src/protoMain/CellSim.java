package protoMain;

import sim.engine.*;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import ec.util.MersenneTwisterFast;
import protoCells.Cell;
import protoCells.CellA;
import protoCells.CellB;
/**
 * The main SimState class
 * @author Chris Keown
 *
 */
public class CellSim extends SimState{
	private static final long serialVersionUID = 1L;
	/**
	 * The total number of steps the simulation runs for
	 */
	public int totalSteps = 500;
	/**
	 * The grid where cells are stored and displayed
	 */
	public SparseGrid2D grid;
	/**
	 * The width of the cell grid
	 */
	private int gridWidth = 50;
	/**
	 * Bag to hold all Type-A cells
	 */
	public Bag cellABag;
	/**
	 * Bag to hold all Type-B cells
	 */
	public Bag cellBBag;
	
	/**
	 * @return the grid's width
	 */
	public int getGridWidth(){
		return gridWidth;
	}
	/**
	 * Change the grid width from within the UI Console
	 * @param width
	 */
	public void setGridWidth(int width){
		gridWidth = width;
	}
	/**
	 * A volatile count of the number of Type-A cells
	 */
	public int getCellABagCount(){
		return cellABag.numObjs;
	}
	/**
	 * A volatile count of the number of Type-B cells
	 */
	public int getCellBBagCount(){
		return cellBBag.numObjs;
	}
	/**
	 * @return the total steps of the simulations
	 */
	public int getTotalSteps(){
		return totalSteps;
	}
	/**
	 * For changing the length of the simulation from within the UI console
	 * @param steps - the new number of steps
	 */
	public void setTotalSteps(int steps){
		totalSteps = steps;
	}
	/**
	 * Simulation Constructor
	 * @param seed
	 */
	public CellSim(long seed){
		super(new MersenneTwisterFast(seed));
	}
	/**
	 * Method called at the start of a simulation, timestep = 0
	 */
	public void start(){
		super.start();
		
		grid = new SparseGrid2D(gridWidth, gridWidth);
		cellABag = new Bag();
		cellBBag = new Bag();
		
		//make 1 A cell
		CellA parentA = new CellA();
		addCellToGrid(parentA);
		cellABag.add(parentA);
		
		//make 1 B cell
		CellB parentB = new CellB();
		addCellToGrid(parentB);
		cellBBag.add(parentB);
		
		
		System.out.println("Starting Prototype Sim");
		System.out.println("Steps: " + totalSteps + "; GridWidth: "+ gridWidth);
		printBags();
		
	}
	/**
	 * Method called when the simulation is finished
	 */
	public void finish(){
		System.out.println("Finishing");
		printBags();
		super.finish();
	}
	/**
	 * Method to print to the console the count of the Bags of each cell type<br>Called at the start and end of a simulation
	 */
	private void printBags(){
		System.out.println("A Cells: " + cellABag.numObjs + ", B Cells: " + cellBBag.numObjs);
	}
	/**
	 * Method to add a newly made cell onto the grid at an empty x,y position<br>The cell's stopper is also set here for convenience
	 * @param cell - the cell to add
	 */
	public void addCellToGrid(Cell cell){
		int x,y;
		do {
			x = newGridRan();
			y = newGridRan();
		}
		while (grid.getObjectsAtLocation(x, y) != null);
		grid.setObjectLocation(cell, x, y);
		
		cell.setStopper(schedule.scheduleRepeating(cell));
	}
	/**
	 * Generate a new random coordinate
	 * @return
	 */
	private int newGridRan(){
		return random.nextInt(gridWidth);
	}
	/**
	 * Main method called to begin a simulation
	 * @param args
	 */
	public static void main(String[] args) {
		doLoop(CellSim.class, args);
		System.exit(0);
	}

}
