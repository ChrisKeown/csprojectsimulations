package protoMain;

import sim.engine.*;
import sim.portrayal.Inspector;
import sim.portrayal.grid.SparseGridPortrayal2D;
import java.awt.Color;
import javax.swing.JFrame;

import protoCells.*;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
/**
 * The main UI class
 * @author Chris Keown
 *
 */
public class CellSimWithUI extends GUIState{
	/**
	 * The simulation display
	 */
	public Display2D display;
	/**
	 * A JFrame to hold the display
	 */
	public JFrame displayFrame;
	/**
	 * How the cell grid is portrayed on the display
	 */
	SparseGridPortrayal2D cellPortrayal = new SparseGridPortrayal2D();
	/**
	 * UI Constructor
	 */
	public CellSimWithUI(){
		super(new CellSim(System.currentTimeMillis()));
	}
	/**
	 * UI Constructor
	 * @param state
	 */
	public CellSimWithUI(SimState state){
		super(state);
	}
	/**
	 * Method to close the UI windows
	 */
	public void quit(){
		super.quit();
		if(displayFrame != null){
			displayFrame.dispose();
		}
		displayFrame = null;
		display = null;
	}
	/**
	 * Method to begin the simulation
	 */
	public void start(){
		super.start();
		setupPortrayals();
	}
	/**
	 * Sub method to set up the portrayals of the cells
	 */
	public void setupPortrayals(){
		cellPortrayal.setField( ((CellSim)state).grid);
		
		cellPortrayal.setPortrayalForClass(CellA.class, new sim.portrayal.simple.OvalPortrayal2D(Color.GREEN));
		cellPortrayal.setPortrayalForClass(CellB.class, new sim.portrayal.simple.OvalPortrayal2D(Color.YELLOW));
	
		display.reset();
		display.repaint();
	}
	/**
	 * Initialize the UI console and display
	 */
	public void init(Controller c){
		super.init(c);
		display = new Display2D(1500,1500,this);
		displayFrame = display.createFrame();
		c.registerFrame(displayFrame);
		displayFrame.setVisible(true);
		
		display.setBackdrop(Color.BLACK);
		display.attach(cellPortrayal, "Cells");
	}
	/**
	 * Main method called when class is ran
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new CellSimWithUI().createController();
	}
	
	public Object getSimulationInspectedObject(){
		return state;
	}
	public Inspector getInspector(){
		Inspector i = super.getInspector();
		i.setVolatile(true);
		return i;
	}

}
