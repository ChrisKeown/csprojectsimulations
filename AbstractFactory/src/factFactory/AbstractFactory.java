package factFactory;

import sim.engine.*;
/**
 * Static class used to create a CellFactory.<br> The Abstract Factory
 * @author Chris Keown
 *
 */
public class AbstractFactory {
	/**
	 * Method to create a factory
	 * @param state - current state of the sim
	 * @return a new CellFactory
	 */
	public static AbstractFactory getFactory(SimState state){
		return new CellFactory(state);
	}
}
