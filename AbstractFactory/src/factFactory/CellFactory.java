package factFactory;

import factCells.*;
import factMain.CellSim;
import sim.engine.*;
/**
 * The cell factory used to create new cells.<br>
 * A Concrete Factory
 * @author Chris
 *
 */
public class CellFactory extends AbstractFactory{
	//private static final long serialVersionUID = 1;
	/**
	 * The current SimState passed as CellSim, so variables and methods can be accessed
	 */
	private CellSim cSim;
	/**
	 * Constructor
	 * @param state - the current state of the sim
	 */
	public CellFactory(SimState state){
		cSim = (CellSim) state;
	}
	/**
	 * Method to create a new A-Type cell
	 * @return the new A cell
	 */
	public Cell createNewCellA(){
		Cell newCell = new CellA();
		addCellToGrid(newCell);
		cSim.cellABag.add(newCell);
		return newCell;
	}
	/**
	 * Method to create a new B-Type Cell
	 * @return the new B cell
	 */
	public Cell createNewCellB(){
		Cell newCell = new CellB();
		addCellToGrid(newCell);
		cSim.cellBBag.add(newCell);
		return newCell;
	}
	/**
	 * Private method to add the passed cell to a free space on the grid<br>
	 * The passed cell is also set as its own stopper on the schedule
	 * @param c - the cell to be placed
	 * @return the cell
	 */
	private Cell addCellToGrid(Cell c){
		int x,y;
		//add cell to cellGrid, where a cell is not currently located
		do {
			x = genRan();
			y = genRan();
		}
		while (cSim.cellGrid.getObjectsAtLocation(x, y) != null);
		cSim.cellGrid.setObjectLocation(c, x, y);
				
		//add cell to schedule
		c.setStopper(cSim.schedule.scheduleRepeating(c));
		return c;
	}
	/**
	 * Generate a random number between 0-grid width
	 * @return the number
	 */
	private int genRan(){
		return cSim.random.nextInt(cSim.getGridWidth());
	}

}
