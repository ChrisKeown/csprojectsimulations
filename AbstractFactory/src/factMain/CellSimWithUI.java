package factMain;

import java.awt.Color;
import javax.swing.JFrame;
import factCells.CellA;
import factCells.CellB;
import sim.display.*;
import sim.engine.*;
import sim.portrayal.Inspector;
import sim.portrayal.grid.SparseGridPortrayal2D;

/**
 * A UI for the simulation
 * @author Chris Keown
 *
 */
public class CellSimWithUI extends GUIState{
	//private static final long serialVersionUID = 1;
	/**
	 * The display
	 */
	public Display2D display;
	/**
	 * The display frame
	 */
	public JFrame displayFrame;
	/**
	 * How the cells and cell grid are portrayed
	 */
	SparseGridPortrayal2D cellPortrayal = new SparseGridPortrayal2D();
	/**
	 * UI Constructor with System.currentTimeMillis as the seed
	 */
	public CellSimWithUI(){
		super(new CellSim(System.currentTimeMillis()));
	}
	/**
	 * Constructor with state as the seed
	 * @param state
	 */
	public CellSimWithUI(SimState state){
		super(state);
	}
	/**
	 * Main method called to start the UI as well as the sim
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new CellSimWithUI().createController();
	}
	/**
	 * Close the UI
	 */
	public void quit(){
		super.quit();
		
		if (displayFrame!=null){
			displayFrame.dispose();
		}
		displayFrame = null;
		display = null;
	}
	/**
	 * Start the UI
	 */
	public void start(){
		super.start();
		setupPortrayals();
	}
	/**
	 * Method to set the portrayals of the two cell types
	 */
	public void setupPortrayals(){
		cellPortrayal.setField( ((CellSim)state).cellGrid );
	
		cellPortrayal.setPortrayalForClass(CellA.class, new sim.portrayal.simple.OvalPortrayal2D(Color.GREEN));
		cellPortrayal.setPortrayalForClass(CellB.class, new sim.portrayal.simple.OvalPortrayal2D(Color.YELLOW));
		
		display.reset();
		display.repaint();
	}
	/**
	 * Initialise the display<br>
	 * Makes the UI easier to start
	 */
	public void init(Controller c){
		super.init(c);
		display = new Display2D(1500, 1500, this);
		displayFrame = display.createFrame();
		c.registerFrame(displayFrame);
		displayFrame.setVisible(true);
		
		display.setBackdrop(Color.BLACK);
		
		display.attach(cellPortrayal, "Cells");
	}
	
	public Object getSimulationInspectedObject(){
		return state;
	}
	public Inspector getInspector(){
		Inspector i = super.getInspector();
		i.setVolatile(true);
		return i;
	}

}
