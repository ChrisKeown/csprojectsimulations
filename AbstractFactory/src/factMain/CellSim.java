package factMain;

import ec.util.MersenneTwisterFast;
import factFactory.*;
import sim.engine.*;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;

/**
 * The main simulation class <br> The Client
 * @author Chris Keown
 *
 */
public class CellSim extends SimState{
	private static final long serialVersionUID = 1;
	/**
	 * The number of steps the simulation runs for<br>Default is 500
	 */
	public int totalSteps = 500;
	/**
	 * The variable used to create cells
	 */
	public CellFactory cellFactory;
	/**
	 * The grid on which cells are stored/located
	 */
	public SparseGrid2D cellGrid;
	/**
	 * The width of the square cell grid<br>
	 */
	private int gridWidth = 50;
	/**
	 * A Bag to store all the A-Type cells
	 */
	public Bag cellABag;
	/**
	 * A Bag to store all the B-Type cells
	 */
	public Bag cellBBag;
	
	/**
	 * Simulation constructor
	 * @param seed
	 */
	public CellSim(long seed){
		super(new MersenneTwisterFast(seed));
	}
	/**
	 * 
	 * @return the grid width
	 */
	public int getGridWidth(){
		return gridWidth;
	}
	/**
	 * For changing the width of the grid on the UI console
	 * @param width
	 */
	public void setGridWidth(int width){
		gridWidth = width;
	}
	/**
	 * For changing the number of steps the simulation runs for on the UI Console
	 * @param steps - the new number of steps
	 */
	public void setTotalSteps(int steps){
		totalSteps = steps;
	}
	/**
	 * @return the number of steps the simulation runs for
	 */
	public int getTotalSteps(){
		return totalSteps;
	}
	/**
	 * A volatile count of the current number of Type-A Cells for the UI Console
	 */
	public int getCellABagCount(){
		return cellABag.numObjs;
	}
	/**
	 * A volatile count of the current number of Type-B Cells for the UI Console
	 */
	public int getCellBBagCount(){
		return cellBBag.numObjs;
	}
	/**
	 * Called to start the simulation (time step = 0). Initiates the factory, grid and two starting cells
	 */
	public void start(){
		super.start();
		
		cellFactory = (CellFactory) AbstractFactory.getFactory(this);
		cellGrid = new SparseGrid2D(gridWidth, gridWidth);
		//for displaying total A/B cells
		cellABag = new Bag();
		cellBBag = new Bag();
		
		//make 1 of each cell - cells do the rest as they are steppable
		cellFactory.createNewCellA();
		cellFactory.createNewCellB();
		
		System.out.println("Starting Abstract Factory Sim");
		System.out.println("Steps: " + totalSteps + "; Grid Width: " + gridWidth);
		printBags();
	}
	/**
	 * Main Method called when the sim is ran
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		doLoop(CellSim.class, args);
		System.exit(0);
	}
	/**
	 * Ends the sim when there is nothing left on the schedule (all cells have stopped)
	 */
	public void finish(){
		System.out.println("Finishing");
		printBags();
		super.finish();
	}
	/**
	 * Console output for the number of cells in each bag
	 */
	private void printBags(){
		System.out.println("A Cells: " + cellABag.numObjs + ", B Cells: " + cellBBag.numObjs);
	}

}
