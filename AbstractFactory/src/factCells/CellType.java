package factCells;
/**
 * Enum for distinguishing cell types
 * @author Chris Keown
 *
 */
public enum CellType {
	A,B;
}
