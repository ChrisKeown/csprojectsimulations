package factCells;

/**
 * The A-type cells <br> A Concrete Product
 * @author Chris Keown
 *
 */
public class CellA extends Cell{
	private static final long serialVersionUID = 1;
	/**
	 * Cell A constructor - sets this.type to enum A
	 */
	public CellA(){
		super(CellType.A);
	}
	/**
	 * Return the cell as a string
	 */
	public String toString(){
		return "A";
	}

}
