package factCells;

import sim.engine.*;
import factMain.CellSim;

/**
 * The abstract cell agents. <br> The Abstract Product.
 * @author Chris Keown
 *
 */
public abstract class Cell implements Steppable, Stoppable{
	private static final long serialVersionUID = 1;
	/**
	 * Variable to stop the agents from being added to the schedule
	 */
	protected Stoppable stopper;
	/**
	 * The cell's type, stored locally
	 */
	private CellType type;
	/**
	 * Method to set the new cell's type, called in the constructor of CellA or CellB
	 * @param t - the cell's new type
	 */
	public void setType(CellType t){
		this.type = t;
	}
	/**
	 * 
	 * @return the cells type
	 */
	public CellType getType(){
		return type;
	}
	/**
	 * 
	 * @return the cells stopper
	 */
	/*
	public Stoppable getStopper(){
		return stopper;
	}*/
	/**
	 * Set the cell's stopper
	 * @param stop - the new stopper
	 */
	public void setStopper(Stoppable stop){
		this.stopper = stop;
	}
	/**
	 * Abstract constructor
	 */
	public Cell(CellType type){
		this.type = type;
	}
	/**
	 * The cells step method. What code is executed when the cell is on the schedule<br>
	 * The cells have a 1% chance to create a same type cell, which is placed randomly on the grid
	 */
	public void step(SimState state){
		CellSim cSim = (CellSim)state;
	
		if (cSim.schedule.getSteps() == cSim.totalSteps){
			this.stop();
		}
		else{
			int ran = cSim.random.nextInt(100);
			if (ran==0){
				if (this.getType() == CellType.A){
					cSim.cellFactory.createNewCellA();
				}
				else if(this.getType() == CellType.B){
					cSim.cellFactory.createNewCellB();
				}
			}
		}
	}
	/**
	 * Method to stop the cell
	 */
	public void stop(){
		stopper.stop();
	}
}
