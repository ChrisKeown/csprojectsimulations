package factCells;
/**
 * The B-Type cells <br> A Concrete Product
 * @author Chris Keown
 *
 */
public class CellB extends Cell{
	private static final long serialVersionUID = 1;

	/**
	 * Cell B Constructor - sets this.type to enum B
	 */
	public CellB(){
		super(CellType.B);
	}
	/**
	 * Returns the cell type as a string
	 */
	public String toString(){
		return "B";
	}

}
