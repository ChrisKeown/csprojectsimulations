package buildMain;

import buildBuilder.CellDirector;
import ec.util.MersenneTwisterFast;
import sim.engine.*;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
/**
 * The main simulation class
 * @author Chris Keown
 *
 */
public class CellSim extends SimState{
	private final static long serialVersionUID = 1;
	/**
	 * The total steps the simulation runs for<br>Default is 500
	 */
	public int totalSteps = 500;
	/**
	 * Variable to hold the cell director, in charge of building the cells
	 */
	public CellDirector cellDirector;
	/**
	 * The grid the cells are stored
	 */
	public SparseGrid2D grid;
	/**
	 * The width of the cell grid
	 */
	private int gridWidth = 50;
	/**
	 * A bag of A-Type cells
	 */
	public Bag cellABag;
	/**
	 * A bag of B-Type cells
	 */
	public Bag cellBBag;
	
	/**
	 * Constructor for the main class
	 * @param seed
	 */
	public CellSim(long seed){
		super(new MersenneTwisterFast(seed));
	}
	/**
	 * 
	 * @return the grid width
	 */
	public int getGridWidth(){
		return gridWidth;
	}
	/**
	 * Change the grid width from within the UI Console
	 * @param width
	 */
	public void setGridWidth(int width){
		gridWidth = width;
	}
	/**
	 * Set the total number of steps the simulation runs for within the UI Console
	 * @param steps
	 */
	public void setTotalSteps(int steps){
		totalSteps = steps;
	}
	/**
	 * @return the total number of steps
	 */
	public int getTotalSteps(){
		return totalSteps;
	}
	//These methods show console errors when ran through eclipse - Bags not initialised before trying to return count
	//Fine once UI is opened
	/**
	 * A volatile count of the number of Type-A cells shown on the UI Console
	 */
	public int getCellABagCount(){
		return cellABag.numObjs;
	}
	/**
	 * A volatile count of the number of Type-B cells shown on the UI Console
	 */
	public int getCellBBagCount(){
		return cellBBag.numObjs;
	}
	
	/**
	 * Method called at the start of a simulation<br>
	 * Initializes the directory, grid and bags. Builds one of each type of cells
	 */
	public void start(){
		super.start();
		
		cellDirector = new CellDirector(this);
		grid = new SparseGrid2D(gridWidth, gridWidth);
		cellABag = new Bag();
		cellBBag = new Bag();
		
		//create 1 of each cell to start
		cellDirector.createA();
		cellDirector.createB();
		System.out.println("Starting Builder Sim");
		System.out.println("Steps: " + totalSteps + "; Grid Wdith: " + gridWidth);
		printBags();
	}
	/**
	 * Method called when there are no more events on the scheduler
	 */
	public void finish(){
		System.out.println("Finishing");
		printBags();
		super.finish();
	}
	/**
	 * Main method called when sim is ran
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		doLoop(CellSim.class, args);
		System.exit(0);
	}
	/**
	 * Console output of the number of each type of cell on the grid
	 */
	private void printBags(){
		System.out.println("A Cells: " + cellABag.numObjs + ", B Cells : "+ cellBBag.numObjs);
	}
}
