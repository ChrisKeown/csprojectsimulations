package buildMain;

import java.awt.Color;
import javax.swing.JFrame;

import buildCells.CellA;
import buildCells.CellB;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.Inspector;
import sim.portrayal.grid.SparseGridPortrayal2D;

/**
 * A UI to visualize the Builder implementation
 * @author Chris Keown
 *
 */
public class CellSimWithUI extends GUIState{
	/**
	 * The display
	 */
	public Display2D display;
	/**
	 * The display frame
	 */
	public JFrame displayFrame;
	/**
	 * The portrayal of the cells on the grid
	 */
	SparseGridPortrayal2D cellPortrayal = new SparseGridPortrayal2D();
	
	
	public CellSimWithUI(){
		super(new CellSim(System.currentTimeMillis()));
	}
	
	public CellSimWithUI(SimState state) {
		super(state);
	}
	/**
	 * Main method ran to start the sim
	 * @param args
	 */
	public static void main(String[] args) {
		new CellSimWithUI().createController();
	}
	/**
	 * Method called to close the UI
	 */
	public void quit(){
		super.quit();
		if (displayFrame != null){
			displayFrame.dispose();
		}
		displayFrame = null;
		display = null;
	}
	/**
	 * Method called to start the UI
	 */
	public void start(){
		super.start();
		setupPortrayals();
	}
	/**
	 * Method called to initialize the UI elements
	 */
	public void setupPortrayals(){
		cellPortrayal.setField( ((CellSim)state).grid);
		
		//cellPortrayal.setPortrayalForAll(new sim.portrayal.simple.OvalPortrayal2D(Color.GREEN));
		cellPortrayal.setPortrayalForClass(CellA.class, new sim.portrayal.simple.OvalPortrayal2D(Color.GREEN));
		cellPortrayal.setPortrayalForClass(CellB.class, new sim.portrayal.simple.OvalPortrayal2D(Color.YELLOW));
		
		display.reset();
		display.repaint();
	}
	/**
	 * Method to ease the construction of UI
	 */
	public void init(Controller c){
		super.init(c);
		display = new Display2D(1500,1500, this);
		displayFrame = display.createFrame();
		c.registerFrame(displayFrame);
		displayFrame.setVisible(true);
		
		display.setBackdrop(Color.BLACK);
		display.attach(cellPortrayal, "Cells");
	}
	
	public Object getSimulationInspectedObject(){
		return state;
	}
	public Inspector getInspector(){
		Inspector i = super.getInspector();
		i.setVolatile(true);
		return i;
	}
	
}
