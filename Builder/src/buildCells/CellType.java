package buildCells;
/**
 * Enum to distinguish between cell types
 * @author Chris Keown
 *
 */
public enum CellType {
	A,B;
}
