package buildCells;

import buildMain.CellSim;
import sim.engine.*;
/**
 * The Concrete Product
 * @author Chris Keown
 *
 */
public class Cell implements Steppable, Stoppable{
	private final static long serialVersionUID = 1;
	/**
	 * The cells type, stored locally
	 */
	private CellType cellType;
	/**
	 * The cells stopper
	 */
	protected Stoppable stopper;
	/**
	 * Cell Constructor
	 */
	public Cell(){
	}

	/**
	 * Set the cells type, as called by the Builder
	 * @param type - the new type
	 */
	public void setType(CellType type){
		this.cellType = type;
	}
	/**
	 * 
	 * @return the cells type
	 */
	public CellType getType(){
		return cellType;
	}
	/**
	 * 
	 * @return the cells stopper
	 */
	/*
	public Stoppable getStopper(){
		return stopper;
	}*/
	/**
	 * Set the cells stopper
	 * @param stop - the new stopper
	 */
	public void setStopper(Stoppable stop){
		this.stopper = stop;
	}
	/**
	 * Stop the cell
	 */
	public void stop(){
		stopper.stop();
	}
	/**
	 * Method to say what the cell does on each time step<Br>
	 * The cell has a 1% chance to create another cell of the same type
	 */
	public void step(SimState state){
		CellSim cSim = (CellSim) state;
		
		if (cSim.schedule.getSteps() == cSim.totalSteps){
			this.stop();
		}
		else {
			int ran = cSim.random.nextInt(100);
			if (ran == 0){
				if (this.getType() == CellType.A){
					cSim.cellDirector.createA();
				}
				else if (this.getType() == CellType.B){
					cSim.cellDirector.createB();
				}
			}
		}
	}
	

}
