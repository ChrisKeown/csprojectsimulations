package buildBuilder;

import buildCells.Cell;
import buildCells.CellType;
import buildMain.CellSim;
import sim.engine.*;
/**
 * The Builder Director<br>
 * For making the concrete products (cells)
 * @author Chris Keown
 *
 */
public class CellDirector{
	/**
	 * The current state of the sim passed as type CellSim to access methods and variables
	 */
	private CellSim cSim;	
	/**
	 * Constructor
	 * @param state
	 */
	public CellDirector(SimState state){
		cSim = (CellSim)state;
	}
	/**
	 * Create a new A-Type cell
	 * @return the new cell
	 */
	public Cell createA(){
		Cell nCell;
		nCell = new CellBuilder().setCellType(CellType.A).createCell();
		addCellToGrid(nCell);
		cSim.cellABag.add(nCell);
		return nCell;
	}
	/**
	 * Create a new B-Type cell
	 * @return the new cell
	 */
	public Cell createB(){
		Cell nCell;
		nCell = new CellBuilder().setCellType(CellType.B).createCell();
		addCellToGrid(nCell);
		cSim.cellBBag.add(nCell);
		return nCell;
	}
	/**
	 * Add the newly made cell onto the cell grid
	 * @param c - the cell to be added
	 * @return the cell
	 */
	private Cell addCellToGrid(Cell c){
		int x,y;
		do {
			x = newRan();
			y = newRan();
		}
		while (cSim.grid.getObjectsAtLocation(x, y) != null);
		cSim.grid.setObjectLocation(c, x, y);
		
		c.setStopper(cSim.schedule.scheduleRepeating(c));
		return c;
	}
	/**
	 * 
	 * @return A random int between 0-gridWidth
	 */
	private int newRan(){
		return cSim.random.nextInt(cSim.getGridWidth());
	}
	
}
