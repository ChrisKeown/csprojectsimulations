package buildBuilder;

import buildCells.Cell;
import buildCells.CellType;
/**
 * The Abstract Builder
 * @author Chris Keown
 *
 */
public interface CellBuilderInt {

	CellBuilderInt setCellType(CellType cType);
	
	Cell createCell();
}
