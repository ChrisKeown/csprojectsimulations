package buildBuilder;

import buildCells.Cell;
import buildCells.CellA;
import buildCells.CellB;
import buildCells.CellType;
/**
 * The Concrete Builder
 * @author Chris Keown
 *
 */
public class CellBuilder implements CellBuilderInt{
	private Cell cell;
	/**
	 * Constructor to make a new cell
	*/
	public CellBuilder(){
		cell = new Cell();
	}
	/**
	 * Set the new cell's type
	 */
	@Override
	public CellBuilderInt setCellType(CellType type){
		if (type == CellType.A){
			cell = new CellA();
		}
		else if (type == CellType.B){
			cell = new CellB();
		}
		return this;
	}
	/**
	 * Return the newly made cell
	 */
	@Override
	public Cell createCell(){
		return cell;
	}
}
