package genFactory;

import sim.engine.SimState;
/**
 * The Abstract Factory used to create the Concrete Factory
 * @author Chris Keown
 *
 */
public class AbstractFactory {
	/**
	 * The static method to create a new Factory
	 * @param state
	 * @return a new factory
	 */
	public static AbstractFactory getFactory(SimState state){
		return new CellFactory(state);
	}
}
