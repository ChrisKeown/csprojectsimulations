package genFactory;

import genCells.Cell;
import genCells.CellA;
import genCells.CellB;
import genMain.CellSim;
import sim.engine.SimState;
import sim.util.Int2D;
/**
 * The Concrete Factory<br>The class responsible for creating new cells
 * @author Chris Keown
 *
 */
public class CellFactory extends AbstractFactory{
	/**
	 * The state of the sim as a variable
	 */
	private CellSim cSim;
	/**
	 * Constructor
	 * @param state
	 */
	public CellFactory(SimState state){
		cSim = (CellSim)state;
	}
	/**
	 * Method to generate a new coordinate within the limits of the cell grid
	 * @return
	 */
	private int genCoord(){
		return cSim.random.nextInt(cSim.getGridWidth());
	}
	/**
	 * Generate a new location for a cell, checking there isn't already a cell there
	 * @return the location
	 */
	private Int2D checkCoord(){
		Int2D loc;
		do {
			loc = new Int2D(genCoord(), genCoord());
		}
		while (cSim.cellGrid.getObjectsAtLocation(loc.x, loc.y) != null);
		return loc;
	}
	/**
	 * Add the cell to the grid
	 * @param pCell
	 */
	private void addCellToGrid(Cell pCell){
		cSim.cellGrid.setObjectLocation(pCell, pCell.getCellLocation());
	}
	/**
	 * Create a new A-Type cell at the passed location
	 * @param loc - where to put the cell on the grid
	 */
	public void createACellAtLocation(Int2D loc){
		Cell newA = new CellA(loc);
		newA.setStopper(cSim.schedule.scheduleRepeating(newA));
		cSim.cellABag.add(newA);
		addCellToGrid(newA);
	}
	/**
	 * Create a new B-Type cell at the passed location
	 * @param loc - where to put the cell on the grid
	 */
	public void createBCellAtLocation(Int2D loc){
		Cell newB = new CellB(loc);
		newB.setStopper(cSim.schedule.scheduleRepeating(newB));
		cSim.cellBBag.add(newB);
		addCellToGrid(newB);
	}
	/**
	 * Create a random cell (50/50 chance of each)
	 */
	public void createRandomCell(){
		int ran = cSim.random.nextInt(2);
		if(ran == 0){
			createACellAtLocation(checkCoord());
		}
		else{
			createBCellAtLocation(checkCoord());
		}
	}
	
}
