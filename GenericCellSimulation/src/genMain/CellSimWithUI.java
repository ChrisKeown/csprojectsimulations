package genMain;

import java.awt.*;
import javax.swing.*;
import genCells.*;
import sim.display.*;
import sim.engine.*;
import sim.portrayal.Inspector;
import sim.portrayal.grid.*;

/**
 * The main UI class
 * @author Chris Keown
 *
 */
public class CellSimWithUI extends GUIState{
	/**
	 * The simulation display
	 */
	public Display2D display;
	/**
	 * A JFrame to hold the display
	 */
	public JFrame displayFrame;
	/**
	 * How the cells are protrayed onto the display
	 */
	SparseGridPortrayal2D cellPortrayal = new SparseGridPortrayal2D();
	
	/**
	 * Basic UI Constructor
	 */
	public CellSimWithUI(){
		super(new CellSim(System.currentTimeMillis()));
	}
	/**
	 * UI Constructor<br>Mainly for loading a checkpoint
	 * @param state
	 */
	public CellSimWithUI(SimState state){
		super(state);
	}
	/**
	 * Run the sim
	 * @param args
	 */
	public static void main(String[] args) {
		new CellSimWithUI().createController();
	}
	/**
	 * @return The name of the sim
	 */
	public static String getName(){
		return "A Simple Cell Proliferation Simulation";
	}
	
	/**
	 * Method called at the end of a simulation run
	 */
	public void finish(){
		super.finish();
	}
	/**
	 * Method called to close the displays
	 */
	public void quit(){
		super.quit();
		if (displayFrame !=null){
			displayFrame.dispose();
		}
		displayFrame = null;
		display = null;
	}
	/**
	 * Method to start the sim with UI
	 */
	public void start(){
		super.start();
		setupPortrayals();
	}
	/**
	 * Method to set up the cell portrayals<Br>A-Type cells displayed as GREEN ovals, B-Type cells displayed as YELLOW ovals
	 */
	public void setupPortrayals(){
		cellPortrayal.setField( ((CellSim)state).cellGrid);
		
		cellPortrayal.setPortrayalForClass(CellA.class, new sim.portrayal.simple.OvalPortrayal2D(Color.GREEN));
		cellPortrayal.setPortrayalForClass(CellB.class, new sim.portrayal.simple.OvalPortrayal2D(Color.YELLOW));
		
		display.reset();
		display.repaint();
	}
	/**
	 * Method to initialise the UI console
	 */
	public void init(Controller c){
		super.init(c);
		display = new Display2D(1200,1200,this);
		displayFrame = display.createFrame();
		c.registerFrame(displayFrame);
		displayFrame.setVisible(true);
		
		display.attach(cellPortrayal, "Cells");
		display.setBackdrop(Color.BLACK);
		displayFrame.setTitle("The Simulation Display");
	}
	/**
	 * Method to pull the inspected object
	 */
	public Object getSimulationInspectedObject(){
		return state;
	}
	/**
	 * Method to set up an Inspector to monitor variables while the sim is running
	 */
	public Inspector getInspector(){
		Inspector i = super.getInspector();
		i.setVolatile(true);
		return i;
	}
	
	

}
