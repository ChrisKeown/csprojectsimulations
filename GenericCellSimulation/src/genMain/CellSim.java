package genMain;

import ec.util.MersenneTwisterFast;
import genFactory.AbstractFactory;
import genFactory.CellFactory;
import sim.engine.*;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
/**
 * The state of the simulation
 * @author Chris Keown
 *
 */
public class CellSim extends SimState{
	private static final long serialVersionUID = 1L;
	/**
	 * The grid to store the cells
	 */
	public SparseGrid2D cellGrid;
	/**
	 * The factory object responsible for creating new cells
	 */
	public CellFactory cellFactory;
	/**
	 * A bag of all A-Type cells
	 */
	public Bag cellABag;
	/**
	 * A bag of all B-Type cells
	 */
	public Bag cellBBag;
	/**
	 * The width of cellGrid
	 */
	private int gridWidth = 50;
	/**
	 * The number of starting cells
	 */
	private int startingCells = 8;
	/**
	 * The total number of steps the simulation runs for
	 */
	private int totalSteps = 1000;
	/**
	 * A count of the number of divides
	 */
	public int divCount;
	/**
	 * A count of the number of differentiations
	 */
	public int dieCount;
	/**
	 * A count of the number of deaths
	 */
	public int diffCount;
	
	/**
	 * @return the total simulation steps
	 */
	public int getTotalSteps(){
		return totalSteps;
	}
	/**
	 * Method to update the total number of steps
	 * @param steps - the new number of steps
	 */
	public void setTotalSteps(int steps){
		totalSteps = steps;
	}
	/**
	 * @return the width of the grid
	 */
	public int getGridWidth(){
		return gridWidth;
	}
	/**
	 * Method to update the grid width
	 * @param width
	 */
	public void setGridWidth(int width){
		gridWidth = width;
	}
	/**
	 * @return the number of starting cells
	 */
	public int getStartingCells(){
		return startingCells;
	}
	/**
	 * Method to update the number of starting cells
	 * @param num - the new number
	 */
	public void setStartingCells(int num){
		startingCells = num;
	}
	/**
	 * @return the die count
	 */
	public int getDieCount(){
		return dieCount;
	}
	/**
	 * @return the differentiation count
	 */
	public int getDiffCount(){
		return diffCount;
	}
	/**
	 * @return the dividing count
	 */
	public int getDivCount(){
		return divCount;
	}
	
	/**
	 * Main constructor for the sim
	 * @param seed
	 */
	public CellSim(long seed){
		super(new MersenneTwisterFast(seed));
	}
	/**
	 * Method called at the start of a simulation run
	 */
	@Override
	public void start(){
		super.start();
		cellFactory = (CellFactory) AbstractFactory.getFactory(this);
		cellGrid = new SparseGrid2D(gridWidth, gridWidth);
		cellABag = new Bag();
		cellBBag = new Bag();
		
		dieCount = 0;
		divCount = 0;
		diffCount = 0;
		
		for (int i=0;i<startingCells;i++){
			cellFactory.createRandomCell();
		}
		System.out.println("Starting Generic Cell Sim");
		System.out.println("Steps: " + totalSteps + "; Grid Width: " + gridWidth + "; Starting Cells: " + startingCells);
		printBags();
		
	}
	/**
	 * Method called at the end of a simulation
	 */
	@Override
	public void finish(){
		System.out.println("Finishing Sim");
		printBags();
		printCounts();
	}
	/**
	 * Load up the simulation
	 * @param args
	 */
	public static void main(String[] args) {
		doLoop(CellSim.class, args);
		System.exit(0);
	}
	/**
	 * Method for printing the number of each type of cell to the console
	 */
	private void printBags(){
		System.out.println("A cells: " + cellABag.numObjs + ", B cells: " + cellBBag.numObjs);
	}
	/**
	 * Method for printing the number of occurences of each cell action<br>Displayed within the 'Model' tab on the UI console
	 */
	private void printCounts(){
		System.out.println("#Divided: " + divCount);
		System.out.println("#Diff: " + diffCount);
		System.out.println("#Died: " + dieCount);
	}

}
