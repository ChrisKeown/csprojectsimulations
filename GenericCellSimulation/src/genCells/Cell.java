package genCells;

import java.util.ArrayList;

import genMain.CellSim;
import sim.engine.*;
import sim.util.Int2D;

/**
 * The Abstract Product<br>The Cell superclass
 * @author Chris Keown
 *
 */
public abstract class Cell implements Steppable, Stoppable{
	private static final long serialVersionUID = 1L;
	/**
	 * The type of cell
	 */
	private CellType cellType;
	/**
	 * The current state of the cell
	 */
	private CellState cellState;
	/**
	 * The cell's stopper
	 */
	private Stoppable stopper;
	/**
	 * The cells location
	 */
	private Int2D cellLocation;
	
	/**
	 * Super Constructor
	 * @param type - the type of cell to be created
	 * @param loc - the location to put the new cell
	 */
	public Cell(CellType type, Int2D loc){
		this.cellState = CellState.QUOTIENT;
		this.cellType = type;
		this.cellLocation = loc;
		this.stopper = null;
	}
	
	/**
	 * Method to update the cells location<br>UNUSED
	 * @param loc - the new location as an Int2D
	 */
	public void setCellLocation(Int2D loc){
		this.cellLocation = loc;
	}
	/**
	 * Method to update the cell's location<br>UNUSED
	 * @param x - the x coordinate
	 * @param y - the y coordinate
	 */
	public void setCellLocation(int x, int y){
		Int2D newLoc = new Int2D(x,y);
		this.cellLocation = newLoc;
	}
	/**
	 * @return the cells location
	 */
	public Int2D getCellLocation(){
		return cellLocation;
	}
	/**
	 * @return the cell's type
	 */
	public CellType getCellType(){
		return cellType;
	}
	/**
	 * @return the cells current state
	 */
	public CellState getCellState(){
		return cellState;
	}
	/**
	 * Method to set the cell's stopper
	 * @param stop - the new stopper
	 */
	public void setStopper(Stoppable stop){
		this.stopper = stop;
	}
	/**
	 * Method to stop the cells rescheduling themselves onto the scheduler
	 */
	@Override
	public void stop() {
		stopper.stop();
	}
	/**
	 * Method for checking if a coordinate is out of bounds, and updating the coordinate if it is
	 * @param state - current state of the sim
	 * @param coord - the coordinate being checked
	 * @return an (updated) coordinate
	 */
	private int checkBounds(SimState state, int coord){
		int newCoord = coord;
		if (coord<0){
			newCoord+=((CellSim)state).getGridWidth();
		}
		else if (coord>((CellSim)state).getGridWidth()){
			newCoord-=((CellSim)state).getGridWidth();
		}
		return newCoord;
	}
	/**
	 * Method to make a cell divide into two new cells of the same type.<br>The cells are placed in a free spot nearby to the parent cell
	 * @param state
	 * @param freeSpaces - an array of all the free spaces around the parent cell
	 */
	private void divide(SimState state, ArrayList<Int2D> freeSpaces){
		int ran, ran2;
		do {
			ran = ((CellSim)state).random.nextInt(freeSpaces.size());
			ran2 = ((CellSim)state).random.nextInt(freeSpaces.size());
		}
		while (ran == ran2);
		if (this.getCellType() == CellType.A){
			((CellSim)state).cellFactory.createACellAtLocation(freeSpaces.get(ran));
			((CellSim)state).cellFactory.createACellAtLocation(freeSpaces.get(ran2));
		}
		if (this.getCellType() == CellType.B){
			((CellSim)state).cellFactory.createBCellAtLocation(freeSpaces.get(ran));
			((CellSim)state).cellFactory.createBCellAtLocation(freeSpaces.get(ran2));
		}
		//System.out.println("Cell created at (" + freeSpaces.get(ran).x + ", " + freeSpaces.get(ran).y + ")");
		//System.out.println("Cell created at (" + freeSpaces.get(ran2).x + ", " + freeSpaces.get(ran2).y + ")");
		die(state);
	}
	/**
	 * Method to kill a cell agent and stop it recheduling itself
	 * @param state
	 */
	private void die(SimState state){
		if (this.getCellType() == CellType.A){
			((CellSim)state).cellABag.remove(this);
		}
		else if (this.getCellType() == CellType.B){
			((CellSim)state).cellBBag.remove(this);
		}
		
		((CellSim)state).cellGrid.remove(this);
		this.stop();
	}
	/**
	 * Method to convert a cell into the other type
	 * @param state
	 */
	private void differentiate(SimState state){
		if (this.getCellType() == CellType.A){
			((CellSim)state).cellFactory.createBCellAtLocation(this.cellLocation);
		}
		else if (this.getCellType() == CellType.B){
			((CellSim)state).cellFactory.createACellAtLocation(this.cellLocation);
		}
		this.die(state);
	}
	
	/**
	 * The cell's actions on each step of the scheduler<br>Look here: <a href="https://cs.gmu.edu/~eclab/projects/mason/docs/classdocs/sim/engine/Steppable.html">Steppable Javadoc</a>
	 */
	@Override
	public void step(SimState state) {
		if ( ((CellSim)state).schedule.getSteps() == ((CellSim)state).getTotalSteps()){
			this.stop();
		}
		else {
			int rand = ((CellSim)state).random.nextInt(1000);
			//chance to die
			if (rand == 0){
				((CellSim)state).dieCount++;
				die(state);
				/*
				System.out.println("Cell "+this.cellType.toString() + " at (" 
			 				+ this.cellLocation.x + ", "+this.cellLocation.y+") has died");
				*/
			}

			switch (this.cellState){
			case ACTIVE:
				//do nothing/change state
				if (rand>0 && rand<11){
					this.cellState = CellState.QUOTIENT;
					//System.out.println("Cell is now QUOTIENT");
				}

				//differentiate
				if (rand>10 && rand<21){
					((CellSim)state).diffCount++;
					differentiate(state);
					/*
					System.out.println("Cell " + this.getCellType().toString() + " at (" 
					+ this.getCellLocation().x + ", " + this.getCellLocation().y + ") has differentiated");
					 */
				}

				//divide
				if (rand>20 && rand<31){
					/*
					System.out.println("The " + this.getCellType().toString() + " cell at (" 
								+ this.cellLocation.x + ", " + this.cellLocation.y + ") wants to divide");
					 */
					//CHECK NEIGHBOURS
					ArrayList<Int2D> freeSpaces = new ArrayList<Int2D>();
					//check there is room around cell, otherwise don't divide
					for (int i=-1;i<2;i++){
						for (int j=-1;j<2;j++){
							//possible child coords
							int checkX = this.cellLocation.x+i;
							int checkY = this.cellLocation.y+j;
							//check not OOB
							checkX = checkBounds(state, checkX);
							checkY = checkBounds(state, checkY);

							if ( ((CellSim)state).cellGrid.getObjectsAtLocation(checkX, checkY) == null){
								freeSpaces.add(new Int2D(checkX,checkY));
							}
						}
					}
					for (Int2D check : freeSpaces){
						if (check == this.cellLocation){
							freeSpaces.remove(check);
						}
					}
					/*
					System.out.println("There are "+ freeSpaces.size() + " free spaces around cell at (" 
										+ this.cellLocation.x + ", "+this.cellLocation.y+")");
					 */
					if (freeSpaces.size()>=2){
						divide(state, freeSpaces);
						((CellSim)state).divCount++;
					}
					else{
						//do nothing as no room for child cells
						this.cellState = CellState.QUOTIENT;
						//System.out.println("No room for child cells");
						//die(state);
					}
				}
			case QUOTIENT:
				int ranQ = ((CellSim)state).random.nextInt(10);
				//become active
				if (ranQ == 0){
					this.cellState = CellState.ACTIVE;
				}
			}
		}
	}

}
