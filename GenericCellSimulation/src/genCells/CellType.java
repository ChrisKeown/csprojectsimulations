package genCells;
/**
 * Enum for the type of the cell
 * @author Chris Keown
 *
 */
public enum CellType {
	A,B;
}
