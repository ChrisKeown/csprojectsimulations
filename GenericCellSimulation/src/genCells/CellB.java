package genCells;

import sim.util.Int2D;
/**
 * A Concrete Product<br>Cells of Type-B
 * @author Chris Keown
 *
 */
public class CellB extends Cell{
	private static final long serialVersionUID = 1L;
	/**
	 * Constructor
	 * @param location
	 */
	public CellB(Int2D location){
		super(CellType.B, location);
	}
}
