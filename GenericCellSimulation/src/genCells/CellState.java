package genCells;
/**
 * Enum for the current state of the cell
 * @author Chris Keown
 *
 */
public enum CellState {
	ACTIVE,QUOTIENT;
}
