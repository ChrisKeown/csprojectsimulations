package genCells;

import sim.util.Int2D;
/**
 * A Concrete Product<Br>Cells of Type-A
 * @author Chris Keown
 *
 */
public class CellA extends Cell{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param location
	 */
	public CellA(Int2D location){
		super(CellType.A, location);
	}
}
